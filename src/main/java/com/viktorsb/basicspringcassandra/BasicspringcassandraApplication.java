package com.viktorsb.basicspringcassandra;

import com.viktorsb.basicspringcassandra.model.Customer;
import com.viktorsb.basicspringcassandra.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class BasicspringcassandraApplication implements CommandLineRunner {

	@Autowired
	CustomerRepository customerRepository;

	public static void main(String[] args) {
		SpringApplication.run(BasicspringcassandraApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		clearData();    // empty the 'customer' table
		saveData();      // persist Customer entities to Cassandra
		lookup();        // load all records and show all on the console
	}

	public void clearData() {
		customerRepository.deleteAll();
	}

	public void saveData() {
		System.out.println("##-------------------SAVE_CUSTOMERS_TO_CASSANDRA-------------------##");
		Customer cust_1 = new Customer(1, "Vikus", "Belovs", 30);
		Customer cust_2 = new Customer(2, "Jared", "Vennet", 32);
		Customer cust_3 = new Customer(3, "Annette", "Berger", 27);
		Customer cust_4 = new Customer(4, "May", "Go", 22);
		Customer cust_5 = new Customer(5, "Jörg", "Jänssen", 32);
		Customer cust_6 = new Customer(6, "Paloma", "Saloma", 25);

		// saves data to ElasticSearch or Solr
		customerRepository.save(cust_1);
		customerRepository.save(cust_2);
		customerRepository.save(cust_3);
		customerRepository.save(cust_4);
		customerRepository.save(cust_5);
		customerRepository.save(cust_6);
	}

	public void lookup() {
		System.out.println("##----------LOOKUP_CUSTOMERS_FROM_CASSANDRA_BY_FIRSTNAME-----------##");
		List<Customer> vikus = customerRepository.findByFirstName("Vikus");
		vikus.forEach(System.out::println);

		System.out.println("##-------------LOOKUP_CUSTOMERS_FROM_CASSANDRA_BY_AGE--------------##");
		List<Customer> custAgeGreaterThan25 = customerRepository.findCustomerHasAgeGreaterThan(25);
		custAgeGreaterThan25.forEach(System.out::println);
	}
}